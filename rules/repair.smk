rule repair_flexbar:
    input:
        fq1 = join(DIRS["flexbar"], R1),
        fq2 = join(DIRS["flexbar"], R2)
    output:
        p1 = join(DIRS["flexbar"], "repaired/"+R1),
        p2 = join(DIRS["flexbar"], "repaired/"+R2),
        singles = join(DIRS["flexbar"], "repaired/{sample}.singles.fq.gz"),
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    log:
        join(DIR_LOGS, "repair_flexbar/{sample}.stderr"),
    params:
        extra = REPAIR_EXTRA
    shell:
        "repair.sh {params.extra} in={input.fq1} in2={input.fq2} out={output.p1} out2={output.p2} outs={output.singles} overwrite 2> {log}"


rule repair_atropos:
    input:
        fq1 = join(DIRS["atropos"], R1),
        fq2 = join(DIRS["atropos"], R2)
    output:
        p1 = join(DIRS["atropos"], "repaired/"+R1),
        p2 = join(DIRS["atropos"], "repaired/"+R2),
        singles = join(DIRS["atropos"], "repaired/{sample}.singles.fq.gz"),
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    log:
        join(DIR_LOGS, "repair_atropos/{sample}.stderr"),
    params:
        extra = REPAIR_EXTRA
    shell:
        "repair.sh {params.extra} in={input.fq1} in2={input.fq2} out={output.p1} out2={output.p2} outs={output.singles} overwrite 2> {log}"

        
rule repair_cutadapt:
    input:
        fq1 = join(DIRS["cutadapt"], R1),
        fq2 = join(DIRS["cutadapt"], R2)
    output:
        p1 = join(DIRS["cutadapt"], "repaired/"+R1),
        p2 = join(DIRS["cutadapt"], "repaired/"+R2),
        singles = join(DIRS["cutadapt"], "repaired/{sample}.singles.fq.gz"),
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    log:
        join(DIR_LOGS, "repair_cutadapt/{sample}.stderr"),
    params:
        extra = REPAIR_EXTRA
    shell:
        "repair.sh {params.extra} in={input.fq1} in2={input.fq2} out={output.p1} out2={output.p2} outs={output.singles} overwrite 2> {log}"

        
rule repair_fastp:
    input:
        fq1 = join(DIRS["fastp"], R1),
        fq2 = join(DIRS["fastp"], R2)
    output:
        p1 = join(DIRS["fastp"], "repaired/"+R1),
        p2 = join(DIRS["fastp"], "repaired/"+R2),
        singles = join(DIRS["fastp"], "repaired/{sample}.singles.fq.gz"),
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    log:
        join(DIR_LOGS, "repair_fastp/{sample}.stderr"),
    params:
        extra = REPAIR_EXTRA
    shell:
        "repair.sh {params.extra} in={input.fq1} in2={input.fq2} out={output.p1} out2={output.p2} outs={output.singles} overwrite 2> {log}"

        
rule repair_bbduk:
    input:
        fq1 = join(DIRS["bbduk"], R1),
        fq2 = join(DIRS["bbduk"], R2)
    output:
        p1 = join(DIRS["bbduk"], "repaired/"+R1),
        p2 = join(DIRS["bbduk"], "repaired/"+R2),
        singles = join(DIRS["bbduk"], "repaired/{sample}.singles.fq.gz"),
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    log:
        join(DIR_LOGS, "repair_bbduk/{sample}.stderr"),
    params:
        extra = REPAIR_EXTRA
    shell:
        "repair.sh {params.extra} in={input.fq1} in2={input.fq2} out={output.p1} out2={output.p2} outs={output.singles} overwrite 2> {log}"
