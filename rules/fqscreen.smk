rule fqscreen_bbduk:
    input:
        fq1=join(DIRS["bbduk"], R1),
        fq2=join(DIRS["bbduk"], R2)
    output:
        touch(join(DIRS["bbduk"], "fastq-screen/{sample}.done"))
    conda:
        join(DIR_ENVS, "fastqscreen.yaml")
    log:
        join(DIR_LOGS, "fqscreen_bbduk/{sample}.log"),
    threads: 12
    params:
        extra=FQSCREEN_EXTRA,
        conf=FQSCREEN_CONF,
        dir=join(DIRS["bbduk"], "fastq-screen/{sample}")
    shell:
        "fastq_screen {params.extra} "
        "--conf {params.conf} "
        "--threads {threads} "
        "--outdir {params.dir} {input} "
        "> {log} 2>&1"


rule fqscreen_atropos:
    input:
        fq1=join(DIRS["atropos"], R1),
        fq2=join(DIRS["atropos"], R2)
    output:
        touch(join(DIRS["atropos"], "fastq-screen/{sample}.done"))
    conda:
        join(DIR_ENVS, "fastqscreen.yaml")
    log:
        join(DIR_LOGS, "fqscreen_atropos/{sample}.log"),
    threads: 12
    params:
        extra=FQSCREEN_EXTRA,
        conf=FQSCREEN_CONF,
        dir=join(DIRS["atropos"], "fastq-screen/{sample}")
    shell:
        "fastq_screen {params.extra} "
        "--conf {params.conf} "
        "--threads {threads} "
        "--outdir {params.dir} {input} "
        "> {log} 2>&1"


rule fqscreen_fastp:
    input:
        fq1=join(DIRS["fastp"], R1),
        fq2=join(DIRS["fastp"], R2)
    output:
        touch(join(DIRS["fastp"], "fastq-screen/{sample}.done"))
    conda:
        join(DIR_ENVS, "fastqscreen.yaml")
    log:
        join(DIR_LOGS, "fqscreen_fastp/{sample}.log"),
    threads: 12
    params:
        extra=FQSCREEN_EXTRA,
        conf=FQSCREEN_CONF,
        dir=join(DIRS["fastp"], "fastq-screen/{sample}")
    shell:
        "fastq_screen {params.extra} "
        "--conf {params.conf} "
        "--threads {threads} "
        "--outdir {params.dir} {input} "
        "> {log} 2>&1"


rule fqscreen_cutadapt:
    input:
        fq1=join(DIRS["cutadapt"], R1),
        fq2=join(DIRS["cutadapt"], R2)
    output:
        touch(join(DIRS["cutadapt"], "fastq-screen/{sample}.done"))
    conda:
        join(DIR_ENVS, "fastqscreen.yaml")
    log:
        join(DIR_LOGS, "fqscreen_cutadapt/{sample}.log"),
    threads: 12
    params:
        extra=FQSCREEN_EXTRA,
        conf=FQSCREEN_CONF,
        dir=join(DIRS["cutadapt"], "fastq-screen/{sample}")
    shell:
        "fastq_screen {params.extra} "
        "--conf {params.conf} "
        "--threads {threads} "
        "--outdir {params.dir} {input} "
        "> {log} 2>&1"


rule fqscreen_flexbar:
    input:
        fq1=join(DIRS["flexbar"], R1),
        fq2=join(DIRS["flexbar"], R2)
    output:
        touch(join(DIRS["flexbar"], "fastq-screen/{sample}.done"))
    conda:
        join(DIR_ENVS, "fastqscreen.yaml")
    log:
        join(DIR_LOGS, "fqscreen_flexbar/{sample}.log"),
    threads: 12
    params:
        extra=FQSCREEN_EXTRA,
        conf=FQSCREEN_CONF,
        dir=join(DIRS["flexbar"], "fastq-screen/{sample}")
    shell:
        "fastq_screen {params.extra} "
        "--conf {params.conf} "
        "--threads {threads} "
        "--outdir {params.dir} {input} "
        "> {log} 2>&1"

