##
## Copyright 2018-2019, Sebastian Schmeier (schmeier@tuta.io)
##
import glob, os, os.path, datetime
from os.path import join, abspath

## =============================================================================
## LOAD VARIABLES FROM CONFIGFILE
## config-file needs to be submitted on command-line via --configfile
configfile: "config.yaml"  # default

## define global Singularity image for reproducibility
## needs --use-singularity to run all jobs in container
singularity: config["singularity"]

DIR_OUTPUT     = abspath(config["basedir"])
DIR_LOGS       = join(DIR_OUTPUT, "logs")
DIR_BENCHMARKS = join(DIR_OUTPUT, "benchmarks")
DIR_ENVS       = abspath("envs")
DIR_RULES      = abspath("rules")

# results
DIR_RESULTS    = join(DIR_OUTPUT, "results")
DIR_FASTP      = join(DIR_RESULTS, "fastp_processed")
FASTPREPORTS   = join(DIR_RESULTS, "fastp_reports")
DIR_FLEXBAR    = join(DIR_RESULTS, "flexbar_noadapt")
DIR_CUTADAPT   = join(DIR_RESULTS, "cutadapt")
DIR_BBDUK      = join(DIR_RESULTS, "bbduk")
DIR_ATROPOS    = join(DIR_RESULTS, "atropos")
DIR_REPAIR     = join(DIR_RESULTS, "repaired")
DIR_MULTIQC    = join(DIR_RESULTS, "multiqc")

DIR_SAMPLES    = abspath(config["samples"]["dir"])
R1             = config["samples"]["r1"]
R2             = config["samples"]["r2"]

# configs
FASTP_EXTRA    = config["fastp"]["extra"]
FLEXBAR_EXTRA  = config["flexbar"]["extra"]
CUTADAPT_EXTRA = config["cutadapt"]["extra"]
BBDUK_EXTRA    = config["bbduk"]["extra"]
ATROPOS_EXTRA  = config["atropos"]["extra"]
REPAIR_EXTRA   = config["repair"]["extra"]
FQSCREEN_EXTRA = config["fastq_screen"]["extra"] 
FQSCREEN_CONF  = abspath(config["fastq_screen"]["conf"])

# adapters
ADAPTERFILE    = abspath(config["adapters"])

## =============================================================================
## SAMPLES

SAMPLES, = glob_wildcards(join(DIR_SAMPLES, R1))
print("Number of samples in {} to process: {}".format(DIR_SAMPLES, len(SAMPLES)))

## =============================================================================
## TARGETS
DIRS = {"fastp": DIR_FASTP,
        "bbduk": DIR_BBDUK,
        "cutadapt": DIR_CUTADAPT,
        "flexbar": DIR_FLEXBAR,
        "atropos": DIR_ATROPOS}

DIRS_MQC = {"fastp": FASTPREPORTS,
            "bbduk": join(DIR_LOGS, "bbduk"),
            "cutadapt": DIR_CUTADAPT,
            "flexbar": DIR_FLEXBAR,
            "atropos": DIR_ATROPOS}

analyses = list(set(config["analyses"]))
TARGETS = []
do = []
MQCDIRS = []
REPAIR = 0
FQSCREEN = 0
for atype in analyses:
    if atype == "repair":
        REPAIR = 1  
    elif atype == "fastq_screen":
        FQSCREEN = 1
    elif atype not in DIRS:
        continue
    else:
        TARGETS += expand(join(DIRS[atype], R1), sample=SAMPLES)
        MQCDIRS.append(DIRS_MQC[atype])
        do.append(atype)

if REPAIR:
    print("PE repair-mode activated.")
    for atype in do:
        TARGETS += expand(join(DIRS[atype], "repaired/"+R1), sample=SAMPLES)

if FQSCREEN:
    print("PE fastq_screen activated.")
    for atype in do:
        TARGETS += expand(join(DIRS[atype], "fastq-screen/{sample}.done"), sample=SAMPLES)
        MQCDIRS += expand(join(DIRS[atype], "fastq-screen/{sample}"), sample=SAMPLES)

print("Will perform these analyses: {}".format(",".join(do)))

## =============================================================================
## RULES

## Pseudo-rule to state the final targets, so that the whole
## workflow is run.
rule all:
    input:
        [TARGETS, join(DIR_MULTIQC, "multiqc-report.html")]

        
# bbduk adapter and quality trimming
rule bbduk:
    input:
        r1 = join(DIR_SAMPLES, R1),
        r2 = join(DIR_SAMPLES, R2)
    output:
        out1 = join(DIRS["bbduk"], R1),  
        out2 = join(DIRS["bbduk"], R2)
    log:
        join(DIR_LOGS, "bbduk/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.bbduk.txt")
    threads: 4
    conda:
        join(DIR_ENVS, "bbmap.yaml")
    params:
        extra = BBDUK_EXTRA,
        adapters = ADAPTERFILE,
        stats = join(DIR_LOGS, "bbduk/{sample}.stats")
    shell:
        "bbduk.sh {params.extra} "
        "ref={params.adapters} "
        "stats={params.stats} "
        "in1={input.r1} "
        "in2={input.r2} "
        "out1={output.out1} "
        "out2={output.out2} "
        "> {log} 2>&1"
        
        
## Adaptertrimming, etc.
rule fastp:
    input:
        r1 = join(DIR_SAMPLES, R1),
        r2 = join(DIR_SAMPLES, R2)
    output:
        out1 = join(DIRS["fastp"], R1),  
        out2 = join(DIRS["fastp"], R2),
        rHTML = join(FASTPREPORTS, "{sample}.fastp.html"),
        rJSON = join(FASTPREPORTS, "{sample}.fastp.json")
    log:
        join(DIR_LOGS, "fastp/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.fastp.txt")
    threads: 4
    conda:
        join(DIR_ENVS, "fastp.yaml")
    params:
        extra = FASTP_EXTRA,
        rDIR = FASTPREPORTS
    shell:
        "fastp {params.extra} "
        "--html {output.rHTML} "
        "--json {output.rJSON} "
        "--thread {threads} "
        "-i {input.r1} -I {input.r2} "
        "-o {output.out1} -O {output.out2} "
        "> {log} 2>&1"


# really slow
rule cutadapt:
    input:
        r1 = join(DIR_SAMPLES, R1),
        r2 = join(DIR_SAMPLES, R2)
    output:
        out1 = join(DIRS["cutadapt"], R1),  
        out2 = join(DIRS["cutadapt"], R2),
        qc = join(DIRS["cutadapt"], "{sample}.qc.txt")
    log:
        join(DIR_LOGS, "cutadapt/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.cutadapt.txt")
    threads: 12
    conda:
        join(DIR_ENVS, "cutadapt.yaml")
    params:
        extra = CUTADAPT_EXTRA,
        adapters = "file:{}".format(ADAPTERFILE)
    shell:
        "cutadapt {params.extra} "
        "--cores {threads} "
        "-b {params.adapters} "
        "-B {params.adapters} "
        "-o {output.out1} "
        "-p {output.out2} "
        "{input.r1} "
        "{input.r2} "
        "2> {log} > {output.qc}"


# atropos adapter and quality trimming
rule atropos:
    input:
        r1 = join(DIR_SAMPLES, R1),
        r2 = join(DIR_SAMPLES, R2)
    output:
        out1 = join(DIRS["atropos"], R1),  
        out2 = join(DIRS["atropos"], R2)
    log:
        join(DIR_LOGS, "atropos/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.atropos.txt")
    threads: 12
    conda:
        join(DIR_ENVS, "atropos.yaml")
    params:
        extra = ATROPOS_EXTRA,
	adapters = "file:{}".format(ADAPTERFILE)
    shell:
        "atropos trim {params.extra} "
        "--threads {threads} " 
        "-b {params.adapters} "
        "-o {output.out1} "
        "-p {output.out2} "
        "-pe1 {input.r1} "
        "-pe2 {input.r2} "
        "> {log} 2>&1"


rule flexbar_adapters:
    input:
        r1 = join(DIR_SAMPLES, R1),
        r2 = join(DIR_SAMPLES, R2)
    output:
        out1 = join(DIRS["flexbar"], R1),  
        out2 = join(DIRS["flexbar"], R2),
        outlog = join(DIRS["flexbar"], "{sample}.out.log"),
    log:
        join(DIR_LOGS, "flexbar_adapters/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS,"{sample}.flexbar_adapters.txt")
    threads: 4
    conda:
        join(DIR_ENVS, "flexbar.yaml")
    params:
        extra = FLEXBAR_EXTRA,
        adapters = ADAPTERFILE
    shell:
        "flexbar {params.extra} "
        "--threads {threads} "
        "-r {input.r1} "
        "-p {input.r2} "
        "-R {output.out1} "
        "-D {output.out2} "
        "-a {params.adapters} "
        "--zip-output GZ " # compression
        "--stdout-log "
        "--output-log {output.outlog} "
        "> {log} 2>&1"
        
        
rule multiqc:
    input:
        TARGETS
    output:
        join(DIR_MULTIQC, "multiqc-report.html")
    log:
        join(DIR_LOGS, "multiqc/multiqc.log")
    params:
        extra=r"--force", # --quiet
        config = "data/multiqc-conf.yaml", ## specifies filenames to search for
        outdir = DIR_MULTIQC,
        dirs = MQCDIRS
    conda:
        join(DIR_ENVS, "multiqc.yaml")
    shell:
        "multiqc {params.extra} "
        "-c {params.config} "
        "--filename {output} "
        "--outdir {params.outdir} "
        "{params.dirs} "
        "> {log} 2>&1"

        
# some more rules
include: join(DIR_RULES, "repair.smk")
include: join(DIR_RULES, "fqscreen.smk")
