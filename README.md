# Snakemake workflow: NGS preprocess

[![pipeline status](https://gitlab.com/schmeierlab/workflows/ngs-preprocess/badges/v0.3.4/pipeline.svg)](https://gitlab.com/schmeierlab/workflows/ngs-preprocess/-/commits/v0.3.4)

- AUTHOR: Sebastian Schmeier (s.schmeier@pm.me) 
- DATE: 2020-02-19
- VERSION: 0.3.4

[[_TOC_]]

## What?

- Adapter trimming
- Dynamic trimming
- Quality filtering
- Length filtering
- re-pairing of paired-end fqs
- fastq_screen for checking sample species/contaminants on adapter-trimmed data
- etc.

Multiple tools available.

## Recommendations

### Do not use adapter trimming

- If you use a soft-clipping mapper like STAR adapter trimming might not be necessary. BUT, check your mapping rates.

### Adapters unknown

- Use bbduk - bbduk takes a list of adapters (see below for how to obtain a list of adapters). 
- fastp does not use an adapter file but rather tries to infer adpaters from the data, which might be not so precise (untested). However, it is very fast and the quality metrics are great.
- Use both and check mapping rates!

### Adapter known

- Cutadapt is superseded by atropos. Both take a long time to run. 
- Use bbduk or fastp.
- If you really want to use atropos, create a file with only one adapter.

### SmallRNA-seq data

- For smallRNA-seq data likly better to use DNApi [use https://github.com/jnktsj/DNApi](https://github.com/jnktsj/DNApi) for predicting adapter sequence first, then clip it off. However, rule is not implemented yet.

## Available known adapters

A union from several tools: [https://raw.githubusercontent.com/sschmeier/seq_adapters_contaminants/master/adapters_union.fa](https://raw.githubusercontent.com/sschmeier/seq_adapters_contaminants/master/adapters_union.fa)

## Re-pairing (resyncing)

Can be done after trimming using `repair.sh` from BBMap which might use quite a bit of mem.

## fastq_screen

In order to run `fastq_screen`, one need to download or build genome index databases (bowtie2).
The download of common genomes can be accomplished with `fastq_screen --get_genomes`.

Furthermore, a configfile for fastq_screen needs to be created. 
An example is in `data/fastq_screen/fastq_screen.conf`.
This file points to the genome indeces that a fastq_screen run should include.
Specify the path to the configfile in the workflow: `config.yaml`.

## TODO

- Implement DNApi prediction and trimming [https://github.com/jnktsj/DNApi](https://github.com/jnktsj/DNApi)
- Implement rDNA cleaning

## INSTALLATION

```bash
# Install miniconda
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# MACOSX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh

# Add conda dir to PATH
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.zshrc

# Make env
# this environment contains the bare base of what is required to run snakemake
conda env create --name snakemake --file envs/snakemake.yaml
conda activate snakemake

# get repo
git clone --recurse-submodules -j8 git@gitlab.com:schmeierlab/workflows/ngs-preprocess.git
```

## RUNNING THE PIPELINE

### Conda-only mode

```bash
conda activate snakemake

snakemake --use-conda -p --jobs 32 --configfile config.yaml
```

### Singularity-only mode

We provide a Singularity config file in `data/singularity`. However, you can
download automatically the pre-build image from `http://singularity-hub.org` by
specifying in the `config.yaml`:

```bash
singularity: "sschmeier/simg-preprocess:201911"
```

Run the workflow with:

```bash
snakemake --use-singularity --singularity-args "--bind /mnt/data" -p --jobs 32 --configfile config.yaml
```


## TESTING

```bash
git clone https://gitlab.com/schmeierlab/workflows/ngs-test-data.git
snakemake -p --use-singularity --configfile config.yaml
# or
snakemake -p --use-conda --configfile config.yaml
```


## Cluster execution (NESI Mahuika)


### Conda-only mode

```bash
# create one global env
conda env create -n preprocess -f data/nesi/preprocess-nesi-mahuika.yaml

# activate env
conda activate preprocess

mkdir logs

# run snakemake in global env
snakemake -p
          --rerun-incomplete
          -j 999
          --cluster-config data/nesi/cluster-nesi-mahuika.yaml
          --cluster "sbatch -A {cluster.account}
                            -p {cluster.partition}
                            -n {cluster.ntasks}
                            -t {cluster.time}
                            --hint={cluster.hint}
                            --output={cluster.output}
                            --error={cluster.error}
                            -c {cluster.cpus-per-task}"
```

### Singularity-only mode

```bash
snakemake -p
          --rerun-incomplete
          -j 999
          --use-singularity
          --singularity-args "--bind /scale_wlg_nobackup/filesets/nobackup/massey02702"
          --cluster-config data/nesi/cluster-nesi-mahuika.yaml
          --cluster "sbatch -A {cluster.account}
                            -p {cluster.partition}
                            -n {cluster.ntasks}
                            -t {cluster.time}
                            --hint={cluster.hint}
                            --output={cluster.output}
                            --error={cluster.error}
                            -c {cluster.cpus-per-task}"
```
